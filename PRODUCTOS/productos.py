from app import app
from config import mysql
from flask import jsonify
from flask import flash, request

@app.route('/productos/listProveedor', methods=['POST'])
def getProductListByProveedor():
    try:
        _json = request.json
        _proveedor = _json['proveedor']

        if _proveedor and request.method == 'POST':
            sqlQuery = "SELECT PRODUCTO,UNIDAD,IVA FROM ProductosInformacion WHERE PROVEEDOR = %s"
            bindData = (_proveedor)
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute(sqlQuery,bindData)
            list = cursor.fetchall()
            respone = jsonify(list)
            respone.status_code = 200
            return respone

    except Exception as e:
        raise ValueError("{}".format(e))

    finally:
        cursor.close()
        conn.close()
