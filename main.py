import pymysql
from app import app
from config import mysql
from flask import jsonify
from flask import flash, request

from PROVEEDORES import proveedores
from ALBARANES import albaranes
from PRODUCTOS import productos

if __name__ == "main":
    app.run()
