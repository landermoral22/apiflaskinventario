from app import app
from config import mysql
from flask import jsonify
from flask import flash, request

@app.route('/albaranes/insert', methods=['POST'])
def albaran_insert():
	try:
		_json 			= request.json
		_albaran 		= _json[0]['albaran']
		_fecha 			= _json[0]['fecha']
		_proveedor 		= _json[0]['proveedor']
		_totalsiniva 	= _json[0]['totalsiniva']
		_total 			= _json[0]['total']

		informacionAlbaran = [_albaran,_fecha,_proveedor,_totalsiniva,_total]

		if _albaran and _fecha and _proveedor and _totalsiniva and _total and request.method == 'POST':

			diccionarioProductosEntrantes = _json[1]
			_listaProductosEntrantes =[]

			for productoEntrante in diccionarioProductosEntrantes:

				_producto 		= productoEntrante['producto']
				_cantidad 		= productoEntrante['cantidad']
				_unidad 		= productoEntrante['unidad']
				_precio 		= productoEntrante['precio']
				_iva 			= productoEntrante['iva']
				_totalsiniva2 	= productoEntrante['totalsiniva']
				_total2 		= productoEntrante['total']

				listaProductoEntrante = [_producto, _cantidad, _unidad, _precio, _iva, _totalsiniva2, _total2]

				if _producto and _cantidad and _unidad and _precio and _iva and _totalsiniva2 and _total2:
					_listaProductosEntrantes.append(listaProductoEntrante)
				else:
					return empty_parameter(listaProductoEntrante)

			addAlbaranResumen(informacionAlbaran)

			for _productoEntrante in _listaProductosEntrantes:
				addProductoAlbaran(informacionAlbaran,_productoEntrante)
				print("he")
				addProductoDataWithProveedor(informacionAlbaran, _productoEntrante)

			respone = jsonify('Albaran :'+_albaran+' '+_proveedor+' added successfully!')
			respone.status_code = 200
			return respone
		else:
			return empty_parameter2(informacionAlbaran)

	except ValueError as e:
	    respone = jsonify("{}".format(e))
	    return respone

	except Exception as e:
		respone = jsonify("{}".format(e))
		return respone

def addAlbaranResumen(informacion):
	try:
		albaran,fecha,proveedor,totalsiniva,total = informacion[0],informacion[1],informacion[2],informacion[3],informacion[4]

		sqlQuery = "INSERT INTO Albaranes(ALBARAN,FECHA,PROVEEDOR,TOTALSINIVA,TOTAL) VALUES(%s,%s,%s,%s,%s)"

		bindData = (albaran, fecha, proveedor, totalsiniva, total)
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute(sqlQuery, bindData)
		conn.commit()
	except Exception as e:
		raise ValueError("{}".format(e))
	finally:
		cursor.close()
		conn.close()

def addProductoAlbaran(informacion,productoEntrante):
	try:
		albaran, fecha, proveedor, producto, cantidad, unidad, precio, iva, totalsiniva, total = informacion[0], informacion[1], informacion[2], productoEntrante[0],productoEntrante[1],productoEntrante[2],productoEntrante[3],productoEntrante[4],productoEntrante[5],productoEntrante[6]

		print("ALBARAN: "+albaran+" PRODUCTO: "+producto)

		sqlQuery = "INSERT INTO AlbaranesDesglosados(ALBARAN,FECHA,PROVEEDOR,PRODUCTO,CANTIDAD,UNIDAD,PRECIO,IVA,TOTALSINIVA,TOTAL) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
		bindData = (albaran, fecha, proveedor, producto, cantidad, unidad, precio, iva, totalsiniva, total)
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute(sqlQuery, bindData)
		conn.commit()

	except Exception as e:
		raise ValueError("{}".format(e))

	finally:
		cursor.close()
		conn.close()

def addProductoDataWithProveedor(informacion,productoEntrante):
	try:
		producto, proveedor, unidad, iva = productoEntrante[0], informacion[2], productoEntrante[2], productoEntrante[4]

		sqlQuery = "INSERT INTO ProductosInformacion(PRODUCTO,PROVEEDOR,UNIDAD,IVA) VALUES(%s,%s,%s,%s)"

		bindData = (producto,proveedor,unidad,iva)

		print(bindData)
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute(sqlQuery,bindData)
		conn.commit()

	except Exception as e:
		raise ValueError("{}".format(e))

	finally:
		cursor.close()
		conn.close()


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Record not found: ' + request.url,
    }
    respone = jsonify(message)
    respone.status_code = 404
    return respone

def empty_parameter(lista):
	message = {
		'status': 404,
		'message': 'EMPTY PARAMETER ---- '+ 'producto:'+lista[0]+' cantidad:'+str(lista[1])+', unidad:'+lista[2]+', precio:'+str(lista[3])+', iva:'+str(lista[4])+', totalsiniva:'+str(lista[5])+', total:'+str(lista[6])
	}
	respone = jsonify(message)
	respone.status_code = 404
	return respone

def empty_parameter2(lista):
	message = {
		'status': 404,
		'message': 'EMPTY PARAMETER ---- '+ 'albaran:'+lista[0]+' fecha:'+lista[1]+', proveedor:'+lista[2]+', totalsiniva:'+str(lista[3])+', total:'+str(lista[4])
	}
	respone = jsonify(message)
	respone.status_code = 404
	return respone