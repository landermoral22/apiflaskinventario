import pymysql
from app import app
from config import mysql
from flask import jsonify
from flask import flash, request

@app.route('/proveedores/add', methods=['POST'])
def add_proveedor():
	try:
		_json = request.json
		_name = _json['name']
		if _name and request.method == 'POST':
			sqlQuery = "INSERT INTO Proveedores(NAME) VALUES(%s)"
			bindData = (_name)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sqlQuery, bindData)
			conn.commit()
			respone = jsonify('Proveedor added successfully!')
			respone.status_code = 200
			return respone
		else:
			return not_found()
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

@app.route('/proveedores/remove', methods=['DELETE'])
def delete_proveedor():
	try:
		_json = request.json
		_name = _json['name']
		if _name and request.method == 'DELETE':
			sqlQuery = "DELETE FROM Proveedores WHERE NAME=%s"
			bindData = (_name)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sqlQuery, bindData)
			conn.commit()
			respone = jsonify('Proveedor removed successfully!')
			respone.status_code = 200
			return respone
		else:
			return not_found()
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

@app.route('/proveedores/list', methods=['GET'])
def list_proveedor():
	try:
		if request.method == 'GET':
			sqlQuery = "SELECT NAME FROM Proveedores"
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sqlQuery)
			list = cursor.fetchall()
			respone = jsonify(list)
			respone.status_code = 200
			return respone
		else:
			return not_found()
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Record not found: ' + request.url,
    }
    respone = jsonify(message)
    respone.status_code = 404
    return respone